# Mongo Backup Helm Chart
### Description
This helm chart tool helps making backups from mongo instances, encrypting and uploading to s3 buckets.
### Stack used
* [gnupg](https://www.gnupg.org/)
* [mongodb database tools](https://www.mongodb.com/try/download/database-tools)
* [aws cli](https://aws.amazon.com/cli/)
### Parameters
|Name|Description|Value|
|:----|:----|:----:|
|`nameOverride`|String to partially override common.names.fullname template (will maintain the release name)|`""`|
|`fullnameOverride`|String to fully override common.names.fullname template|`""`|
|`namespace`|Resources' namespace|`""`|
|`image.repository`|Name of container image|`""`|
|`image.pullPolicy`|Container image's pull policy|`Always`|
|`image.tag`|Container image's tag|`latest`|
|`registryCredentials.registry`|Registry name|`""`|
|`registryCredentials.username`|Registry username|`""`|
|`registryCredentials.password`|Regsitry password|`""`|
|`imagePullSecrets`|Specify registry secret names as an array|`[]`|
|`daily.enabled`|For running daily cronjob backup|`false`|
|`daily.schedule`|Daily cronjob value|`""`|
|`weekly.enabled`|For running weekly cronjob backup|`false`|
|`weekly.schedule`|Weekly cronjob value|`""`|
|`monthly.enabled`|For running monthly cronjob backup|`false`|
|`monthly.schedule`|Monthly cronjob value|`""`|
|`podAnnotations`|Pod annotation for app name|`"{}"`|
|`serviceAccount.create`|To create a service account|`true`|
|`serviceAccount.annotations`|ServiceAccount Annotations|`"{}"`|
|`serviceAccount.name`|Service account's name|`""`|
|`deployment.vars.USERNAME`|Mongo's username|`""`|
|`deployment.vars.PASSWORD`|Mongo's password|`""`|
|`deployment.vars.HOST`|Mongo's host|`""`|
|`deployment.vars.PORT`|(optional) Mongo's port, default port: 27017|`27017`|
|`deployment.vars.GPG_KEY`|Encryption public key|`""`|
|`deployment.vars.GPG_RECIPIENT`|Encryption recipient|`""`|
|`deployment.vars.S3_PATH`|Bucket path in format "s3://bucket_name/dir/"|`""`|
|`deployment.vars.S3_PROVIDER`|S3 cloud provider,can be aws or generic, default: aws|`generic`|
|`deployment.vars.S3_ENDPOINT`|S3 provider endpoint, required if S3_PROVIDER set to generic value|`""`|
|`deployment.vars.AWS_ACCESS_KEY_ID`|S3 provider access key id|`""`|
|`deployment.vars.AWS_SECRET_ACCESS_KEY`|S3 provider secret access key|`""`|
|`deployment.vars.AWS_DEFAULT_REGION`|S3 provider default region|`""`|

### Restore from backup
In order to restore from backup first donwload from s3 bucket, decrypt the backup with required credentials and install mongodb database tools, run the following commands:
```sh
$ gpg --output <backup.tar.gz> --decrypt <backupfile.tar.gz.gpg>
$ mongorestore --uri="mongodb://<USERNAME>:<PASSWORD>@<HOST>:<PORT>" --authenticationDatabase=admin --gzip --archive=<backup.tar.gz>
```


### Especial env var GPG_KEY
One way of uploading the multiline string GPG_KEY value to helm with the --set flag is the following:
```sh
$ export MULTILINE_VALUE=$(cat <<EOF                     
-----BEGIN PGP PUBLIC KEY BLOCK-----


-----END PGP PUBLIC KEY BLOCK-----
EOF
)
$ helm install -n mongo-backup backup --set deployment.vars.GPG_KEY="$MULTILINE_VALUE" .
```